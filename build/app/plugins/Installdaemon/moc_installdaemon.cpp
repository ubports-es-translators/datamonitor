/****************************************************************************
** Meta object code from reading C++ file 'installdaemon.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../plugins/Installdaemon/installdaemon.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'installdaemon.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Installdaemon_t {
    QByteArrayData data[14];
    char stringdata0[173];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Installdaemon_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Installdaemon_t qt_meta_stringdata_Installdaemon = {
    {
QT_MOC_LITERAL(0, 0, 13), // "Installdaemon"
QT_MOC_LITERAL(1, 14, 9), // "installed"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 7), // "success"
QT_MOC_LITERAL(4, 33, 11), // "uninstalled"
QT_MOC_LITERAL(5, 45, 18), // "isInstalledChanged"
QT_MOC_LITERAL(6, 64, 11), // "isInstalled"
QT_MOC_LITERAL(7, 76, 17), // "onInstallFinished"
QT_MOC_LITERAL(8, 94, 8), // "exitCode"
QT_MOC_LITERAL(9, 103, 20), // "QProcess::ExitStatus"
QT_MOC_LITERAL(10, 124, 10), // "exitStatus"
QT_MOC_LITERAL(11, 135, 19), // "onUninstallFinished"
QT_MOC_LITERAL(12, 155, 7), // "install"
QT_MOC_LITERAL(13, 163, 9) // "uninstall"

    },
    "Installdaemon\0installed\0\0success\0"
    "uninstalled\0isInstalledChanged\0"
    "isInstalled\0onInstallFinished\0exitCode\0"
    "QProcess::ExitStatus\0exitStatus\0"
    "onUninstallFinished\0install\0uninstall"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Installdaemon[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x06 /* Public */,
       4,    1,   52,    2, 0x06 /* Public */,
       5,    1,   55,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    2,   58,    2, 0x08 /* Private */,
      11,    2,   63,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      12,    0,   68,    2, 0x02 /* Public */,
      13,    0,   69,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    6,

 // slots: parameters
    QMetaType::Void, QMetaType::Int, 0x80000000 | 9,    8,   10,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 9,    8,   10,

 // methods: parameters
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Installdaemon::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Installdaemon *_t = static_cast<Installdaemon *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->installed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->uninstalled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->isInstalledChanged((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 3: _t->onInstallFinished((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QProcess::ExitStatus(*)>(_a[2]))); break;
        case 4: _t->onUninstallFinished((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QProcess::ExitStatus(*)>(_a[2]))); break;
        case 5: _t->install(); break;
        case 6: _t->uninstall(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (Installdaemon::*_t)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Installdaemon::installed)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (Installdaemon::*_t)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Installdaemon::uninstalled)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (Installdaemon::*_t)(const bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Installdaemon::isInstalledChanged)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject Installdaemon::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Installdaemon.data,
      qt_meta_data_Installdaemon,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Installdaemon::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Installdaemon::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Installdaemon.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Installdaemon::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void Installdaemon::installed(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Installdaemon::uninstalled(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Installdaemon::isInstalledChanged(const bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
