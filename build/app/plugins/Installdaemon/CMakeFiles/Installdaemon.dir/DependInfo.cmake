# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/matteo/dataMonitor/build/app/plugins/Installdaemon/Installdaemon_automoc.cpp" "/home/matteo/dataMonitor/build/app/plugins/Installdaemon/CMakeFiles/Installdaemon.dir/Installdaemon_automoc.cpp.o"
  "/home/matteo/dataMonitor/plugins/Installdaemon/installdaemon.cpp" "/home/matteo/dataMonitor/build/app/plugins/Installdaemon/CMakeFiles/Installdaemon.dir/installdaemon.cpp.o"
  "/home/matteo/dataMonitor/plugins/Installdaemon/plugin.cpp" "/home/matteo/dataMonitor/build/app/plugins/Installdaemon/CMakeFiles/Installdaemon.dir/plugin.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_DBUS_LIB"
  "QT_GUI_LIB"
  "QT_NETWORK_LIB"
  "QT_NO_DEBUG"
  "QT_QML_LIB"
  "QT_QUICK_LIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/arm-linux-gnueabihf/qt5"
  "/usr/include/arm-linux-gnueabihf/qt5/QtQml"
  "/usr/include/arm-linux-gnueabihf/qt5/QtNetwork"
  "/usr/include/arm-linux-gnueabihf/qt5/QtCore"
  "/usr/lib/arm-linux-gnueabihf/qt5/mkspecs/linux-g++"
  "/usr/include/arm-linux-gnueabihf/qt5/QtQuick"
  "/usr/include/arm-linux-gnueabihf/qt5/QtGui"
  "/usr/include/arm-linux-gnueabihf/qt5/QtDBus"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
