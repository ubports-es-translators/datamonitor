import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Installdaemon 1.0

Page {
    id: settingPage

    header: PageHeader {
        id: pageHeader
        title: i18n.tr("Settings")

    }
    Column {
        anchors.top: pageHeader.bottom
        Row{
            id: firstRow
            Button {
                id: deleteDaemon
                text: i18n.tr("Remove daemon")
                width: units.gu(20)
                color: UbuntuColors.graphite
                onClicked: {
                    if(settings.isDaemonInstalled)
                    {
                        message.visible = false;
                        Installdaemon.uninstall();
                        if(Installdaemon.isInstalled) {
                          settings.isDaemonInstalled=true;
                        }
                        else {
                          settings.isDaemonInstalled=false;
                        }
                    }
                }
            }
        }
        Row{
          Label {
            id: message
            visible: false
            width: units.gu(20)
            height: units.gu(30)
          }
        }
     }
     Connections {
       target: Installdaemon

       onUninstalled: {
           message.visible = true;
           if (success) {
               message.text = i18n.tr("Daemon files removed. DataMonitor is offline.\nClose and open again the 'dataMonitor' app to get daemon files\nautomatically installed again.");
               message.color = UbuntuColors.red;
           }
           else {
               message.text = i18n.tr("Failed to uninstall");
               message.color = UbuntuColors.green;
           }
       }
     }

}
