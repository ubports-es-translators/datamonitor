# dataMonitor

This is a first conceptual app for UBports - Ubuntu Touch OS to bring to this platform the data usage monitoring capability, exploiting the QNetworkConfigurationManager API.

The present app is still in alpha development stage and its current version is 0.0.6.

!!!WARNING: the dataMonitor app works UNCONFINED, due to the need of getting the data statistics from the Network and to the employment of a daemon service, which is designed to execute the Actiondaemon binary every a determined time step, in order to have the received Mbytes database always updated.

The received Mbytes database is automatically generated once installed the daemon files and run the Actiondaemon binary for the first time (after phone re-boot). The daemon runs in background, so it could be possible that it could eventually have an impact on battery discharge rate (I will run some tests later to prove the actual impact). If you want to stay on the safe side and you want to avoid any remote battery drainage, you can delete the daemon files only by opening the Settings page and tapping on the 'remove daemon' button; remember that every time you open the dataMonitor app again, the daemon files will be automatically restored back.

Useful directories:

- app main directory is /opt/click.ubuntu.com/datamonitor.matteobellei;

- mattdaemon.conf and mattdaemon-service.conf are stored in /.config/upstart;

- mattdaemon.log and mattdaemon-service.log are stored in /.cache/upstart;

- received Mbytes database is stored in /.local/share/datamonitor.matteobellei/Databases;

- additional service .conf files are stored in /.config/datamonitor.matteobellei.
