install(FILES "mattdaemon.conf" DESTINATION ${DATA_DIR}daemon)
install(FILES "mattdaemon-service.conf" DESTINATION ${DATA_DIR}daemon)
install(FILES "install.sh" DESTINATION ${DATA_DIR}daemon)
install(FILES "uninstall.sh" DESTINATION ${DATA_DIR}daemon)

#set(QT_IMPORTS_DIR "lib/${ARCH_TRIPLET}")
#add_executable(script install.sh)

#add_custom_command(TARGET script
#        POST_BUILD
#        COMMAND /bin/sh ${QT_IMPORTS_DIR}/install.sh
#        )
