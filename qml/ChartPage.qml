import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Components.Pickers 1.3

/* Thanks to: https://github.com/jwintz/qchart.js for QML bindings for Charts.js */
import "QChart.js" as Charts

/* replace the 'incomplete' QML API U1db with the low-level QtQuick API */
import QtQuick.LocalStorage 2.0

/* Import our javascrit files */
import "ChartUtils.js" as ChartUtils
import "DateUtils.js"  as DateUtils


/*
  NEW: page: added for chapter about "QML and charts"
  Page that display a monthly received Mbytes chart and his legend
*/
Page {
     id: chartPage
     visible: false

     /* default is today, after is updated when the user chose a date with the TimePicker */
     property string targetDate : Qt.formatDateTime(new Date(), "yyyy-MM-dd");

     header: PageHeader {
        title: i18n.tr("Analytic page")
     }

     Component.onCompleted: {

         /* extract the year, month, day from the variable 'targetDate' that contains a value like yyyy-mm-dd */
         var dateParts = chartPage.targetDate.split("-");

         /* build a JS Date Object using string tokens (month is 0-based) */
         var date = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);

         /* calculates first and last day of the month */
         var firstDayMonth = new Date( date.getFullYear(),date.getMonth(), 1);
         var lastDayMonth = new Date( date.getFullYear(), date.getMonth() + 1, 0);

         /* set the data-set at the chart and make visible the chart and legend */
         mBytesChart.chartData = ChartUtils.getChartData(firstDayMonth,lastDayMonth);

         mBytesChartContainer.visible = true;

     }

     Column{
        id: chartPageMainColumn
        spacing: units.gu(2)
        anchors.fill: parent

        /* transparent placeholder: to place the content under the header */
        Rectangle {
            color: "transparent"
            width: parent.width
            height: units.gu(6)
        }

        Row{
            id: monthSelectorRow
            anchors.horizontalCenter: parent.horizontalCenter
            spacing:units.gu(2)

            /* Create a PopOver containing a DatePicker */
            Component {
                id: popoverTargetMonthPicker
                Popover {
                    id: popoverDatePicker

                    DatePicker {
                        id: timePicker
                        mode: "Months|Years"
                        minimum: {
                            var time = new Date()
                            time.setFullYear('2000')
                            return time
                        }
                        /* when Datepicker is closed, is updated the date shown in the button */
                        Component.onDestruction: {
                            targetMonthSelectorButton.text = Qt.formatDateTime(timePicker.date, "MMMM yyyy");
                            chartPage.targetDate = Qt.formatDateTime(timePicker.date, "yyyy-MM-dd");
                        }
                    }
                }
            }

            Label{
                id: targetMonthLabel
                anchors.verticalCenter: targetMonthSelectorButton.verticalCenter
                text: i18n.tr("Month:")
            }

            /* open the popOver component with DatePicker */
            Button {
                id: targetMonthSelectorButton
                width: units.gu(20)
                text: Qt.formatDateTime(new Date(), "MMMM yyyy")
                onClicked: {
                    PopupUtils.open(popoverTargetMonthPicker, targetMonthSelectorButton)
                }
            }

            Button {
                id: showChartButton
                width: units.gu(14)
                text: i18n.tr("Show Chart")
                onClicked: {
                    /* extract the year, month, day from the variable 'targetDate' that contains a value like yyyy-mm-dd */
                    var dateParts = chartPage.targetDate.split("-");

                    /* build a JS Date Object using string tokens (month is 0-based) */
                    var date = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);

                    /* calculates first and last day of the month */
                    var firstDayMonth = new Date( date.getFullYear(),date.getMonth(), 1);
                    var lastDayMonth = new Date( date.getFullYear(), date.getMonth() + 1, 0);

                    /* set the data-set at the chart and make visible the chart and legend */
                    mBytesChart.chartData = ChartUtils.getChartData(firstDayMonth,lastDayMonth);

                    mBytesChartContainer.visible = true;
                }
            }
        }

        //---------------- Chart ---------------------
        Grid {
            id: chartGridContainer
            visible: true
            columns:2                     // this property doesn't seem to be effective on the chart
            columnSpacing: units.gu(1)    // this property doesn't seem to be effective on the chart
            width: parent.width;
            height: parent.height;

            Rectangle {
                id: mBytesChartContainer
                // visible: false
//                visible: true    //Ubuntu.Components.Themes.Ambiance/SuruDark/SuruGradient
//                color: theme.palette.normal.base;
                width: parent.width;
//                height: parent.height - units.gu(20);
                height: parent.height - units.gu(15);

                property string globalTheme;
                property string gridColor;
                property string lineColor;
                property string fontColor;

                /* The monthly MBytes chart */
                QChart{
                    id: mBytesChart;
                    width: parent.width;
                    height: parent.height;
                    chartAnimated: false;
                    /* for all the options see: QChart.js */
                    chartOptions: {"barStrokeWidth": 0,
                                   "scaleFontColor": mBytesChartContainer.fontColor,
                                   "scaleLineColor": mBytesChartContainer.lineColor,
                                   "scaleGridLineColor": mBytesChartContainer.gridColor
                                  };
                    /* chartData: set when the user press 'Show Chart' button */
                    chartType: Charts.ChartType.BAR;
                }
                Component.onCompleted: {
                  globalTheme = Theme.name;
                  if (globalTheme == "Ubuntu.Components.Themes.Ambiance") {
                    mBytesChartContainer.visible = false;
                    gridColor = "rgba(0,0,0,.05)";
                    lineColor = "rgba(0,0,0,.1)";
                    fontColor = "#666";
                  } else if (globalTheme == "Ubuntu.Components.Themes.SuruDark") {
                    mBytesChartContainer.visible = true;
                    gridColor = "rgba(255,255,255,0.1)";
                    lineColor = "rgba(255,255,255,1)";
                    fontColor = "rgba(255,255,255,1)";
                    mBytesChartContainer.color = "black";
                  } else if (globalTheme == "Ubuntu.Components.Themes.SuruGradient") {
                    mBytesChartContainer.visible = true;
                    gridColor = "rgba(255,0,255,0.1)";
                    lineColor = "rgba(255,0,255,1)";
                    fontColor = "rgba(255,255,0,1)";
                    mBytesChartContainer.color = UbuntuColors.purple;
                  } else {
                    mBytesChartContainer.visible = false;
                    gridColor = "rgba(0,0,0,.05)";
                    lineColor = "rgba(0,0,0,.1)";
                    fontColor = "#666";
                  }
                }
            }
        }
    }

}
