import QtQuick 2.4
import QtQuick.LocalStorage 2.0
import "Storage.js" as Storage
import "DateUtils.js" as DateUtils

Item {

	function createDatabase()
	{
			Storage.createTables();
  }

/*	function todayDate()
	{
			var actual_date = Qt.formatDateTime(new Date(), "dd MMMM yyyy");
			var targetDate = new Date (actual_date);
			var formatted_date = DateUtils.formatDateToString(targetDate);
			return formatted_date;
  }

	function todayCurBytes()
	{
			var actual_date = Qt.formatDateTime(new Date(), "dd MMMM yyyy");
			var todayBytes = Storage.getBytesValueByDate(actual_date);
			return todayBytes;
  } */

	function todayPrevBytes(todayPos)
	{
			var actual_date = Qt.formatDateTime(new Date(), "dd MMMM yyyy");
			var todayBytes = Storage.getBytesValueByDateAndPos(actual_date, todayPos);
			return todayBytes;
	}

	function todayLastPos()
	{
			var actual_date = Qt.formatDateTime(new Date(), "dd MMMM yyyy");
			var todayPos = Storage.getTodayLastPosition(actual_date);
			return todayPos;
  }

	function subtractNumbers(data1, data2)
	{
		var sum = data1 - data2;
		return sum;
  }

	function sumNumbers(data1, data2)
	{
		var sum = data1 + data2;
		return sum;
  }

/*	function confrontDates(todayDate)
	{
		var actual_date = Qt.formatDateTime(new Date(), "dd MMMM yyyy");
		var targetDate = new Date (actual_date);
		var formatted_date = DateUtils.formatDateToString(targetDate);
		if (todayDate==formatted_date) {
			 var result = true;
		}
		else {
			 var result = false;
		}
		return result;
  } */

	function storeNewData(data, lastData)
	{
		 var bytes = data/1048576;
		 print("Received Mbytes: " + bytes + " MB")
     var actual_date = Qt.formatDateTime(new Date(), "dd MMMM yyyy");
     var readLastBytes = Storage.getBytesValueByDate(actual_date);
		 var totBytes = bytes + lastData;
		 var sumData = totBytes - readLastBytes;
		 if (totBytes>0 & sumData>0)
		 {
		 		var error = Storage.insertBytesData(actual_date,totBytes);
		 }
		 return totBytes;
	}

	function fillVacantDays()
	{
    var actual_date = Qt.formatDateTime(new Date(), "dd MMMM yyyy");
		var firstOfMonth = DateUtils.firstOfMonth(actual_date);
		var daysToFirst = DateUtils.getDifferenceInDays(firstOfMonth,actual_date);
    var readLastBytes = 0;
		var counter = -1;
		var days = 0;
		if (daysToFirst>0) {
				while (readLastBytes == 0 & days<daysToFirst) {
						var prevDate = DateUtils.addDaysAndFormat(actual_date, counter);
						var readLastBytes = Storage.getBytesValueByDate(prevDate);
						counter = counter -1;
					  days = days +1;
				}
		}
		if (days >= 2 & readLastBytes>0) {
			for (var i=1; i < days; i++) {
					var lastDate = DateUtils.addDaysAndFormat(prevDate, i);
					var error = Storage.insertBytesData(lastDate,readLastBytes);
			}
		}
		var readBytesToday = Storage.getBytesValueByDate(actual_date);
		if (readBytesToday == 0) {
			 var error = Storage.insertBytesData(actual_date,readLastBytes);
		}
		return readLastBytes;
	}

}
