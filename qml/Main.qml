import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Installdaemon 1.0

//import QtQuick.LocalStorage 2.0
//import "Storage.js" as Storage

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'datamonitor.matteobellei'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Settings {
        id:settings
//        category: "MainApp"
        /* flag to show or not the App configuration popup */
//        property bool isFirstUse : true;
        property bool isDaemonInstalled : false;
    }

    Component {
       id: settingPage
       PrefPage{}
    }

    Component {
       id: chartPage
       ChartPage{}
    }

    PageStack {
        id: pageStack

        /* set the firts page of the application */
        Component.onCompleted: {
            pageStack.push(mainPage);
        }

    Page {
        id: mainPage
        anchors.fill: parent

        header: PageHeader {
            id: pageHeader
            title: i18n.tr('Data usage monitor')

            trailingActionBar.actions: [
              Action {
                  text: i18n.tr("Settings")
                  onTriggered: pageStack.push(settingPage)
                  iconName: "settings"
              }
            ]

            StyleHints	{
              foregroundColor:	UbuntuColors.orange
//              backgroundColor:	UbuntuColors.porcelain
              dividerColor:	UbuntuColors.slate
              }
            }
            Component.onCompleted: {

//                if(settings.isFirstUse)
//                {
//                    Storage.createTables();
//
//                    settings.isFirstUse = false;
//
//                }
                if(!settings.isDaemonInstalled)
                {
                    message.visible = false;
                    Installdaemon.install();
                    if(Installdaemon.isInstalled) {
                      settings.isDaemonInstalled=false;
                    }
                    else {
                      settings.isDaemonInstalled=true;
                    }
                }
            }

        Column {
            anchors.top: pageHeader.bottom
            Row{
                id:chartRow
                Button {
                    id:showChartButton
                    text: i18n.tr("Chart")
                    width: units.gu(20)
//                    color: UbuntuColors.green
                    onClicked: {
                        pageStack.push(chartPage)
                    }
                }
            }
            Row{
              Label {
                id: message
                visible: false
                width: units.gu(20)
                height: units.gu(30)
              }
            }
         }
    }
  }
  Connections {
    target: Installdaemon

    onInstalled: {
        message.visible = true;
        if (success) {
            message.text = i18n.tr("Daemon automatically installed.\nPlease reboot the phone, to put the data usage monitoring\napplication online.");
            message.color = UbuntuColors.green;
        }
        else {
            message.text = i18n.tr("Failed to install");
            message.color = UbuntuColors.red;
        }
    }
  }

//  function showSettings() {

//    var settingPage = pageStack.push(PrefPage.qml)

//   }
}
