#include <QNetworkSession>
#include <QNetworkConfigurationManager>

#include "networkdaemon.h"

quint64 Networkdaemon::recBytes() {
  quint64 bytes = 0;
  QNetworkConfigurationManager manager;
  manager.updateConfigurations();
/*  for (int i=0; i < 1000; ++i) {
    continue;
  } */
//  QNetworkConfiguration cfg;
  QNetworkConfiguration cfg = manager.defaultConfiguration();
//  cfg.state();
  QList<QNetworkConfiguration> activeConfigs = manager.allConfigurations(QNetworkConfiguration::Active);
  int conta = 0;
  for (int i=0; i < activeConfigs.count(); ++i) {
      qDebug() << activeConfigs.value(conta).bearerTypeName();
      qDebug() << activeConfigs.value(conta).identifier();
      conta = conta + 1;
  //    cfg = manager.configurationFromIdentifier(activeConfigs.value(1).identifier());
  //    qDebug() << cfg.isValid();
  }
//  const bool Datastat = (manager.capabilities() & QNetworkConfigurationManager::DataStatistics);
//  qDebug() << Datastat;
  QNetworkSession *session = new QNetworkSession(cfg);
  session->open();
  session->waitForOpened(1000);
  if (session->isOpen()) {
    qDebug() << "Session is open!";
    if (session->state()==session->Connected) {
      qDebug() << "Session connected!";
    } else {
        if (session->state()==session->NotAvailable) {
          qDebug() << "Session not available.";
        } else {
          if (session->state()==session->Connecting) {
            qDebug() << "Session connecting...";
          } else {
            if (session->state()==session->Invalid) {
              qDebug() << "Session not valid.";
            } else {
              if (session->state()==session->Closing) {
                qDebug() << "Session closing...";
              } else {
                if (session->state()==session->Disconnected) {
                  qDebug() << "Session disconnected.";
                } else {
                  if (session->state()==session->Roaming) {
                    qDebug() << "Session is roaming...";
                  } else {
                    qDebug() << "Session state not identified...";
                  }
                }
              }
            }
          }
      }
    }
  } else {
    qDebug() << "Session is still closed.";
  }
  bytes = session->bytesReceived();
/*  QString ident = session->sessionProperty("UserChoiceConfiguration").toString();
  QString ident2 = session->sessionProperty("ConnectInBackground").toString();
  QString ident3 = session->sessionProperty("AutoCloseSessionTimeout").toString();
  qDebug() << ident;
  qDebug() << ident2;
  qDebug() << ident3;
  qDebug() << session->state(); */
/*  QString error = session->errorString();
  qDebug() << error;
  qDebug() << session->NoBackgroundTrafficPolicy;
  qDebug() << session->NotAvailable;
  qDebug() << session->Connecting;
  qDebug() << session->Connected;
  qDebug() << session->Disconnected;
  qDebug() << session->Roaming; */
//  session->deleteLater();
  return bytes;
}

bool Networkdaemon::isOnline() {

    QNetworkConfigurationManager mgr;  // codice trovato all'indirizzo internet http://doc.qt.io/qt-5/qnetworkconfigurationmanager.html#onlineStateChanged
    mgr.updateConfigurations();
    QNetworkConfiguration cfg = mgr.defaultConfiguration();
    if (cfg.state() == QNetworkConfiguration::Active)
       {
         return true;
       }
    else {
         return false;
         }

}
