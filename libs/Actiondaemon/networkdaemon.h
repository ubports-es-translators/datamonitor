#ifndef NETWORKDAEMON_H
#define NETWORKDAEMON_H

#include <QObject>
#include <QNetworkConfigurationManager>

class Networkdaemon {

public:
    quint64 recBytes();
    bool isOnline();

};


#endif
