#include <QGuiApplication>
#include <QUrl>
#include <QString>
#include <QQuickView>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QObject>
#include <iostream>
#include <fstream>
//#include <QDir>
//#include <QQmlEngine>
#include "networkdaemon.h"

using namespace std;

int main(int argc, char *argv[])
{
    QGuiApplication *app = new QGuiApplication(argc, (char**)argv);
//    qDebug() << "dataMonitor daemon action";
    Networkdaemon ricevuti;
    quint64 bytes = 0;
    bool isOnline = false;
    QVariant prevBytes = 0;
    QVariant prevTodayBytes = 0;
    QVariant totBytes = 0;
//    QVariant todayIs = "1900-01-01";
    QVariant tLastPos = 0;
//    QVariant todayBytes = 0;
    QVariant sumBytes = 0;
    QVariant subBytes = 0;
//    QVariant resultDate = false;
//    string dataStr;
    int todayPosition;
//    string todayStr;
    string todayIsUnity8;
    isOnline = ricevuti.isOnline();
    if (isOnline) {
        qDebug() << "Connection is online.";
      }
    else {
        qDebug() << "Connection is offline.";
      }
    bytes = ricevuti.recBytes();
    if (bytes<0.125) {   // 1 byte = 8 bit therefore 1/8=0.125 is the minimum acceptable actual value
        bytes=0;
    }
    qDebug() << "Bytes received:" << bytes << "bytes";
    fstream rebootedFile;
    rebootedFile.open(".config/datamonitor.matteobellei/rebooted.conf",ios::in);
    rebootedFile>>todayIsUnity8;
    QString todayIsUnity8Str = QString::fromStdString(todayIsUnity8);
    rebootedFile.close();
    fstream todayPosFile;
    todayPosFile.open(".config/datamonitor.matteobellei/todayPos.conf",ios::in);
    if(!todayPosFile) {
      qDebug()<<"Error in reading todayPos.conf file..";
      todayPosFile.open(".config/datamonitor.matteobellei/todayPos.conf",fstream::out);  // line needed to create the file for the first time
      todayPosFile<<"0";
      qDebug()<<"File .config/datamonitor.matteobellei/todayPos.conf created.";
    }
    todayPosFile.close();
/*  fstream tBytesFile;
    tBytesFile.open(".config/datamonitor.matteobellei/todayBytes.conf",ios::in);
    if(!tBytesFile) {
      qDebug()<<"Error in reading todayBytes.conf file..";
      tBytesFile.open(".config/datamonitor.matteobellei/todayBytes.conf",fstream::out);  // line needed to create the file for the first time
      tBytesFile<<"0";
      qDebug()<<"File .config/datamonitor.matteobellei/todayBytes.conf created";
    }
    tBytesFile.close();
    fstream dateFile;
    dateFile.open(".config/datamonitor.matteobellei/todayDate.conf",ios::in);
    if(!dateFile) {
      qDebug()<<"Error in reading todayDate.conf file..";
      dateFile.open(".config/datamonitor.matteobellei/todayDate.conf",fstream::out);  // line needed to create the file for the first time
      dateFile<<"1900-01-01";
      qDebug()<<"File .config/datamonitor.matteobellei/todayDate.conf created";
    }
    dateFile.close(); */
    QQmlEngine engine;
    engine.setOfflineStoragePath(QStringLiteral("/home/phablet/.local/share/datamonitor.matteobellei"));
    QQmlComponent component(&engine,
        QUrl(QStringLiteral("/opt/click.ubuntu.com/datamonitor.matteobellei/current/qml/Daemon.qml")));
//    QFile file("/home/phablet/.local/share/datamonitor.matteobellei/Databases");
    if (component.isError()) {
        qWarning() << component.errors();
    }
    else {
         QObject *object = component.create();
//         if (!file.exists()) {
//           qDebug() << "Storage file doesn't exist. It will be created one.";
         QMetaObject::invokeMethod(object, "createDatabase");
//         }
         QMetaObject::invokeMethod(object, "fillVacantDays",
              Q_RETURN_ARG(QVariant, prevBytes));
         if (prevBytes.isNull()) {
            prevBytes=0;
         }
//         qDebug()<< "prevBytes:" << prevBytes.toDouble() << "MB";
         qDebug()<< "Yesterday stored bytes:" << prevBytes.toDouble() << "MB";
//         if (bytes==0) {
         if (todayIsUnity8Str=="unity8") {
                    QMetaObject::invokeMethod(object, "todayLastPos",
                                       Q_RETURN_ARG(QVariant, tLastPos));
                    if (tLastPos>0) {
                        qDebug()<<"Phone re-booted after the first start-up today.";
                    } else {
                        qDebug()<<"Phone first start-up today.";
                        todayPosFile.open(".config/datamonitor.matteobellei/todayPos.conf",fstream::out);  // line needed to create the file for the first time
                        todayPosFile<<"0";
                    }
                    QString saveLastPos = tLastPos.toString();
                    string saveLastPosStr = saveLastPos.toUtf8().constData();
                    todayPosFile.open(".config/datamonitor.matteobellei/todayPos.conf",fstream::out);
                    todayPosFile<<saveLastPosStr;
                    todayPosFile.close();
/*                    QMetaObject::invokeMethod(object, "todayDate",
                            Q_RETURN_ARG(QVariant, todayIs));
                    qDebug()<< "Today is:" << todayIs.toString();
                    QMetaObject::invokeMethod(object, "todayCurBytes",
                            Q_RETURN_ARG(QVariant, todayBytes));
                    if (todayBytes.isNull()) {
                       todayBytes=0;
                    }
                    qDebug()<< "todayBytes:" << todayBytes.toDouble() << "MB";
                    QMetaObject::invokeMethod(object, "subtractNumbers",
                            Q_RETURN_ARG(QVariant, subBytes),
                            Q_ARG(QVariant, todayBytes),
                            Q_ARG(QVariant, prevBytes));
                    qDebug()<< "subBytes:" << subBytes.toDouble() << "MB";
                    if (subBytes>0) {
                        qDebug()<<"Phone re-booted after the first start-up today.";
                        QString saveBytes = subBytes.toString();
                        string saveBytesStr = saveBytes.toUtf8().constData();
                        tBytesFile.open(".config/datamonitor.matteobellei/todayBytes.conf",fstream::out);
                        tBytesFile<<saveBytesStr;
                        tBytesFile.close();
                        QString saveDate = todayIs.toString();
                        string saveDateStr = saveDate.toUtf8().constData();
                        cout << "Date today: " << saveDateStr;
                        dateFile.open(".config/datamonitor.matteobellei/todayDate.conf",fstream::out);
                        dateFile<<saveDateStr;
                        dateFile.close();
                    } */
         }
//         if (bytes>0) {
         else if (todayIsUnity8Str=="mattdaemon-service") {
            todayPosFile.open(".config/datamonitor.matteobellei/todayPos.conf",ios::in);
            todayPosFile>>todayPosition;
            if (todayPosition>0) {
//              QVariant todayPositionBack = QVariant::convert(todayPosition);
              QVariant todayPositionBack = todayPosition;
              QMetaObject::invokeMethod(object, "todayPrevBytes",
                      Q_RETURN_ARG(QVariant, prevTodayBytes),
                      Q_ARG(QVariant, todayPositionBack));
              qDebug()<< "Today stored bytes:" << prevBytes.toDouble() << "MB";
              QMetaObject::invokeMethod(object, "subtractNumbers",
                      Q_RETURN_ARG(QVariant, subBytes),
                      Q_ARG(QVariant, prevTodayBytes),
                      Q_ARG(QVariant, prevBytes));
            }
/*            tBytesFile.open(".config/datamonitor.matteobellei/todayBytes.conf",ios::in);
            tBytesFile>>dataStr;
            QString readData = QString::fromStdString(dataStr);
            QVariant numberBack = readData.toDouble();
            tBytesFile.close();
            dateFile.open(".config/datamonitor.matteobellei/todayDate.conf",ios::in);
            dateFile>>todayStr;
            dateFile.close();
            QVariant todayActDate = QString::fromStdString(todayStr);
            QMetaObject::invokeMethod(object, "confrontDates",
                Q_RETURN_ARG(QVariant, resultDate),
                Q_ARG(QVariant, todayActDate));
            if (resultDate==true) { */
                 QMetaObject::invokeMethod(object, "sumNumbers",
                          Q_RETURN_ARG(QVariant, sumBytes),
                          Q_ARG(QVariant, prevBytes),
//                          Q_ARG(QVariant, numberBack));
//                          Q_ARG(QVariant, prevTodayBytes));
                          Q_ARG(QVariant, subBytes));
                 QMetaObject::invokeMethod(object, "storeNewData",
                          Q_RETURN_ARG(QVariant, totBytes),
                          Q_ARG(QVariant, bytes),
                          Q_ARG(QVariant, sumBytes));
/*             }
             else {
                 QMetaObject::invokeMethod(object, "storeNewData",
                    Q_RETURN_ARG(QVariant, totBytes),
                    Q_ARG(QVariant, bytes),
                    Q_ARG(QVariant, prevBytes));
             } */
         }
         delete object;
    }

    return 0;
}
